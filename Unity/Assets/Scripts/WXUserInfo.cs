﻿using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class WXUserInfo
{
    public string openid;
    public string nickname;
    public int sex;
    public string language;
    public string city;
    public string province;
    public string country;
    public string headimgurl;
    public List<string> privilege = new List<string>();
    public string unionid;
}