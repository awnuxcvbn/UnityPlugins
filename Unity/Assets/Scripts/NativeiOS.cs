﻿#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class NativeiOS
{
#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void GetLaunchInfo(string objectName, string methodName);

    [DllImport("__Internal")]
    private static extern void InsertPhoto(string imgName);

    [DllImport("__Internal")]
    private static extern void PhotoTake(string objectName, string methodName);

    [DllImport("__Internal")]
    private static extern void PickPhoto(string objectName, string methodName);

    [DllImport("__Internal")]
    private static extern void WxInit(string appId);

    [DllImport("__Internal")]
    private static extern void WxLogin(string objectName, string methodName);

    [DllImport("__Internal")]
    private static extern void WxShareText(string content, int scene);

    [DllImport("__Internal")]
    private static extern void WxShareImg(string imgName, int scene);

    [DllImport("__Internal")]
    private static extern void WxShareUrl(string title, string description, string imgName, string url, int scene);
#endif

    public void iGetLaunchInfo(string objName, string methodName)
    {
#if UNITY_IOS
        GetLaunchInfo(objName, methodName);
#endif
    }

    public void iPhotoInsert(string imgName)
    {
#if UNITY_IOS
        InsertPhoto(imgName);
#endif
    }

    public void iPhotoTake(string objectName, string methodName)
    {
#if UNITY_IOS
        PhotoTake(objectName, methodName);
#endif
    }

    public void iPhotoPick(string objectName, string methodName)
    {
#if UNITY_IOS
        PickPhoto(objectName, methodName);
#endif
    }

    /// <summary>
    /// 微信api初始化
    /// </summary>
    /// <param name="appId"></param>
    /// <param name="appSecret"></param>
    public void iWxInit(string appId)
    {
#if UNITY_IOS
        WxInit(appId);
#endif
    }

    /// <summary>
    /// 微信授权登录
    /// </summary>
    public void iWxLogin(string objectName, string methodName)
    {
#if UNITY_IOS
        WxLogin(objectName, methodName);
#endif
    }

    /// <summary>
    /// 微信分享文字 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void iWxShareText(string content, int scene)
    {
#if UNITY_IOS
        WxShareText(content, scene);
#endif
    }

    /// <summary>
    /// 微信分享图片 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void iWxShareImg(string imgName, int scene)
    {
#if UNITY_IOS
        WxShareImg(imgName, scene);
#endif
    }

    /// <summary>
    /// 微信分享链接 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void iWxShareUrl(string url, string title, string description, string imgName, int scene)
    {
#if UNITY_IOS
        WxShareUrl(url, title, description, imgName, scene);
#endif
    }
}