package cn.net.xuefei.king.plugins;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;

//微信
public class WeChat
{
    private Context m_context;
    private String m_objName = "";
    private String m_methodName = "";
    private String m_filePath = "";
    private IWXAPI m_api;

    public void init(Context context, String app_id)
    {
        m_context = context;
        m_api = WXAPIFactory.createWXAPI(context, app_id, false);
        //将应用的appid注册到微信
        m_api.registerApp(app_id);

        m_filePath = Environment.getExternalStorageDirectory()
                + "/Android/data/" + context.getPackageName() + "/files";
        File destDir = new File(m_filePath);
        if (!destDir.exists())
        {
            destDir.mkdirs();
        }
    }

    //微信登录
    public void login(String objName, String methodName) {
        if (!isWXAppInstalled()) {
            return;
        }
        m_objName = objName;
        m_methodName = methodName;

        SendAuth.Req req = new SendAuth.Req();
        //授权域 获取用户个人信息则填写snsapi_userinfo
        req.scope = "snsapi_userinfo";
        //用于保持请求和回调的状态 可以任意填写
        req.state = m_objName+";"+m_methodName;
        m_api.sendReq(req);
    }

    //微信分享文字 scene分享类型 好友==0 朋友圈==1
    public void shareText(String text, int scene)
    {
        if (!isWXAppInstalled())
        {
            return;
        }
        //初始化WXTextObject对象，填写对应分享的文本内容
        WXTextObject textObject = new WXTextObject();
        textObject.text = text;
        //初始化WXMediaMessage消息对象，
        WXMediaMessage message = new WXMediaMessage();
        message.mediaObject = textObject;
        message.description = text;
        //构建一个Req请求对象
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());   //transaction用于标识请求
        req.message = message;
        req.scene = scene;      //分享类型 好友==0 朋友圈==1
        //发送请求
        m_api.sendReq(req);
    }

    //微信分享图片 imgName沙盒中的文件名含后缀 scene分享类型 好友==0 朋友圈==1
    public void shareImg(String imgName, int scene)
    {
        if (!isWXAppInstalled())
        {
            return;
        }
        if (imgName=="")
        {
            return;
        }
        Bitmap bitmap = BitmapFactory.decodeFile(m_filePath + "/" + imgName);
        WXImageObject wxImageObject = new WXImageObject(bitmap);
        WXMediaMessage message = new WXMediaMessage();
        message.mediaObject = wxImageObject;

        Bitmap thunmpBmp = Bitmap.createScaledBitmap(bitmap, 50, 50, true);
        bitmap.recycle();
        message.thumbData = bmpToByteArray(thunmpBmp, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = message;
        req.scene = scene;
        m_api.sendReq(req);
    }

    //微信分享链接
    public void shareUrl(String url, String title, String description, String imgName, int scene) {
        if (!isWXAppInstalled())
        {
            return;
        }
        Bitmap bitmap = BitmapFactory.decodeFile(m_filePath + "/" + imgName);
        WXWebpageObject wxWebpageObject = new WXWebpageObject();
        wxWebpageObject.webpageUrl = url;

        WXMediaMessage wxMediaMessage = new WXMediaMessage(wxWebpageObject);
        wxMediaMessage.title = title;
        wxMediaMessage.description = description;
        Bitmap thunmpBmp = Bitmap.createScaledBitmap(bitmap, 50, 50, true);
        bitmap.recycle();
        wxMediaMessage.thumbData = bmpToByteArray(thunmpBmp, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = wxMediaMessage;
        req.scene = scene;

        m_api.sendReq(req);
    }

    //判断微信是否安装
    private boolean isWXAppInstalled()
    {
        if (!m_api.isWXAppInstalled())
        {
            Toast.makeText(m_context, "请先安装微信应用", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle)
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle)
        {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try
        {
            output.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return result;
    }
}