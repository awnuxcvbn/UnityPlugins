#import "PhotoInsert.h"  
@implementation PhotoInsert  
- ( void ) imageSaved: ( UIImage *) image didFinishSavingWithError:( NSError *)error   
    contextInfo: ( void *) contextInfo  
{  
    NSLog(@"保存结束");  
    if (error != nil) {  
        NSLog(@"有错误");  
    }  
}  
void InsertPhoto(char *readAddr)  
{  
    NSString *strReadAddr = [NSString stringWithUTF8String:readAddr];  
    UIImage *img = [UIImage imageWithContentsOfFile:strReadAddr];  
    NSLog([NSString stringWithFormat:@"w:%f, h:%f", img.size.width, img.size.height]);  
    PhotoInsert *instance = [PhotoInsert alloc];  
    UIImageWriteToSavedPhotosAlbum(img, instance,   
        @selector(imageSaved:didFinishSavingWithError:contextInfo:), nil);  
}  
@end  